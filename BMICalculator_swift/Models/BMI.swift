//
//  BMI.swift
//  BMICalculator_swift
//
//  Created by Anton Medvediev on 23/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import UIKit

struct BMI{
    let value: Double
    let sex: String
    let text: String
}
