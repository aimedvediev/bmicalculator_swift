//
//  CalculatorBrain.swift
//  BMICalculator_swift
//
//  Created by Anton Medvediev on 23/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import Foundation

struct CalculatorBrain{
    
    var bmi: BMI?
    
    mutating func calculateBMI(height: Double, weight: Double){
        let bmiValue = weight/pow(height,2)
        var text = "Overweight"
        if(bmiValue <= 18.5){
            text = "Underweight"
        }else if(bmiValue<=24.9){
            text = "Normal"
        }
        bmi = BMI(value: bmiValue, sex: "Man", text: text )
    }
    
    func getBMI()-> String{
        return String(format:"%.1f",bmi?.value ?? 0.0)
    }
    func getText()->String{
        return bmi?.text ?? "Error"
    }
    
}
