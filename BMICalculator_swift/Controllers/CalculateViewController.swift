//
//  ViewController.swift
//  BMICalculator_swift
//
//  Created by Anton Medvediev on 19/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import UIKit

class CalculateViewController: UIViewController {
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var wieghtLabel: UILabel!
    @IBOutlet weak var womenButton: UIButton!
    @IBOutlet weak var manButton: UIButton!
    var height = 0.0
    var weight = 0.0
    
    var calculatorBrain = CalculatorBrain()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func heightSlider(_ sender: UISlider) {
        height = Double(sender.value)
        heightLabel.text = String(format:"%.2f", height)+"m"
    }
    
    @IBAction func wieghtSlider(_ sender: UISlider) {
        weight = Double(sender.value)
        wieghtLabel.text = String(format:"%.0f", weight)+"kg"
    }
    
    @IBAction func womenRadioButton(_ sender: UIButton) {
        womenButton.setTitleColor(UIColor.init(red: 0.952639, green: 0.744958, blue: 0.712832, alpha: 1), for: .normal)
        manButton.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func manRadioButton(_ sender: UIButton) {
        manButton.setTitleColor(UIColor.init(red: 0.952639, green: 0.744958, blue: 0.712832, alpha: 1), for: .normal)
        womenButton.setTitleColor(.white, for: .normal)
    }
    
    @IBAction func calculatePressed(_ sender: UIButton) {
        calculatorBrain.calculateBMI(height: height,weight:weight)
        
        self.performSegue(withIdentifier: "goToResult", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult"{
            let destinationVC = segue.destination as! ResultViewController
            destinationVC.bmiValue = calculatorBrain.getBMI()
            destinationVC.bmiText = calculatorBrain.getText()
            
        }
        
    }
}

