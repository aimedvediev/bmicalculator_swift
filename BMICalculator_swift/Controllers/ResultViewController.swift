//
//  ResultViewController.swift
//  BMICalculator_swift
//
//  Created by Anton Medvediev on 23/03/2020.
//  Copyright © 2020 Anton Medvediev. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {

    var bmiValue: String?
    var bmiText: String?
    
    @IBOutlet weak var bmiLabel: UILabel!
    @IBOutlet weak var resultTextLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        bmiLabel.text = bmiValue
        resultTextLabel.text = bmiText
        // Do any additional setup after loading the view.
    }
    @IBAction func recalculatePressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
